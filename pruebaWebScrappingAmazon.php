<?php
// URL of the webpage you want to scrape
$url = "https://www.amazon.es/AMD-Ryzen-5-5600X-Box/dp/B08166SLDF/ref=asc_df_B08166SLDF/?tag=googshopes-21&linkCode=df0&hvadid=469836810464&hvpos=&hvnetw=g&hvrand=1732828253307301046&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9061046&hvtargid=pla-1038719530487&psc=1";

// Send a GET request to the URL and retrieve the HTML code
$html = file_get_contents($url);

// Load the HTML code into a DOM object
$dom = new DOMDocument();
@$dom->loadHTML($html);

// Use XPath to search for elements in the DOM
$xpath = new DOMXPath($dom);

// Find the desired information using XPath expressions
$title = $xpath->query("//*[@id='productTitle']")->item(0)->nodeValue;
$price = $xpath->query("//*[@id='corePriceDisplay_desktop_feature_div']/div[1]/span[2]/span[2]")->item(0)->nodeValue;


// Output the results
echo "Title: $title\n";
echo "Price: $price\n";

?>